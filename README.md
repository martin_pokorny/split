# Simple Legion application with splitting layout constraints

Doesn't really do much, but it illustrates what I think I need from
Legion to interface my pipeline application with the tensor-core
correlator library. Here the copy to the second layout is via another
logical region to make it explicit, but in my pipeline application it
would be implicit by letting Legion do the transformation when copying
the data to an instance in GPU memory.

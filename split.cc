#include <cstdio>
#include <cassert>
#include <cstdlib>
#include "legion.h"
using namespace Legion;

enum TaskIDs {
  TOP_LEVEL_TASK_ID,
  WRITE_SAMPLES_TASK_ID,
  REFORMAT_SAMPLES_TASK_ID,
};

enum FieldIDs {
  FID_VALUE,
};

#define BLOCK_SIZE 128
#define NUM_BLOCKS 200

using value_t = uint16_t;

void
top_level_task(
  const Task *task,
  const std::vector<PhysicalRegion> &regions,
  Context ctx,
  Runtime *runtime) {

  Rect<4> rect(Point<4>::ZEROES(), Point<4>(BLOCK_SIZE*NUM_BLOCKS - 1, 1, 49, 31));
  IndexSpace is = runtime->create_index_space(ctx, rect);
  FieldSpace fs = runtime->create_field_space(ctx);
  {
    FieldAllocator allocator =
      runtime->create_field_allocator(ctx, fs);
    allocator.allocate_field(sizeof(value_t), FID_VALUE);
  }

  LogicalRegion lr_in = runtime->create_logical_region(ctx, is, fs);
  LogicalRegion lr_out = runtime->create_logical_region(ctx, is, fs);

  {
    TaskLauncher write_launcher(WRITE_SAMPLES_TASK_ID, TaskArgument());
    write_launcher.add_region_requirement(
      RegionRequirement(lr_in, WRITE_DISCARD, EXCLUSIVE, lr_in));
    write_launcher.region_requirements[0].add_field(FID_VALUE);
    runtime->execute_task(ctx, write_launcher);
  }
  {
    TaskLauncher reformat_launcher(REFORMAT_SAMPLES_TASK_ID, TaskArgument());
    reformat_launcher.add_region_requirement(
      RegionRequirement(lr_in, READ_ONLY, EXCLUSIVE, lr_in));
    reformat_launcher.region_requirements[0].add_field(FID_VALUE);
    reformat_launcher.add_region_requirement(
      RegionRequirement(lr_out, WRITE_DISCARD, EXCLUSIVE, lr_out));
    reformat_launcher.region_requirements[1].add_field(FID_VALUE);
    runtime->execute_task(ctx, reformat_launcher);
  }
  runtime->destroy_logical_region(ctx, lr_in);
  runtime->destroy_logical_region(ctx, lr_out);
  runtime->destroy_field_space(ctx, fs);
  runtime->destroy_index_space(ctx, is);
}

void
write_samples_task(
  const Task *task,
  const std::vector<PhysicalRegion> &regions,
  Context ctx, Runtime *runtime) {
  assert(regions.size() == 1);
  assert(task->regions.size() == 1);
  assert(task->regions[0].privilege_fields.size() == 1);

  const FieldAccessor<
    WRITE_DISCARD,value_t,4,coord_t,
    Realm::AffineAccessor<value_t,4,coord_t>>
    acc(regions[0], FID_VALUE);
  Rect<4> rect =
    runtime->get_index_space_domain(
      ctx,
      task->regions[0].region.get_index_space());
  for (PointInRectIterator<4> pir(rect); pir(); pir++)
    acc[*pir] =
      (pir[0] + 1) * (pir[1] + 1) * (pir[2] + 1) * (pir[3] + 1);
}

void
reformat_samples_task(
  const Task *task,
  const std::vector<PhysicalRegion> &regions,
  Context ctx, Runtime *runtime) {
  assert(regions.size() == 2);
  assert(task->regions.size() == 2);

  const FieldAccessor<
    READ_ONLY,value_t,4,coord_t,
    Realm::AffineAccessor<value_t,4,coord_t>>
    acc_in(regions[0], FID_VALUE);
  const FieldAccessor<
    WRITE_DISCARD,value_t,4,coord_t,
    Realm::AffineAccessor<value_t,4,coord_t>>
    acc_out(regions[1], FID_VALUE);
  Rect<4> rect =
    runtime->get_index_space_domain(
      ctx,
      task->regions[0].region.get_index_space());
  for (PointInRectIterator<4> pir(rect); pir(); pir++)
    acc_out[*pir] = acc_in[*pir];
}

int
main(int argc, char **argv) {
  Runtime::set_top_level_task_id(TOP_LEVEL_TASK_ID);

  LayoutConstraintID initial_layout;
  {
    OrderingConstraint order(true/*contiguous*/);
    order.ordering.push_back(LEGION_DIM_W);
    order.ordering.push_back(LEGION_DIM_Z);
    order.ordering.push_back(LEGION_DIM_Y);
    order.ordering.push_back(LEGION_DIM_X);
    order.ordering.push_back(LEGION_DIM_F);
    LayoutConstraintRegistrar registrar;
    registrar.add_constraint(order);
    initial_layout = Runtime::preregister_layout(registrar);
  }

  LayoutConstraintID xcorr_layout;
  {
    OrderingConstraint order(true/*contiguous*/);
    order.ordering.push_back(LEGION_INNER_DIM_W);
    order.ordering.push_back(LEGION_DIM_Z);
    order.ordering.push_back(LEGION_DIM_X);
    order.ordering.push_back(LEGION_OUTER_DIM_W);
    order.ordering.push_back(LEGION_DIM_Y);
    order.ordering.push_back(LEGION_DIM_F);
    // not certain that the initialization of SplittingConstraint is
    // correct, but the intention is to have block size BLOCK_SIZE on
    // LEGION_INNER_DIM_X, and the size of LEGION_OUTER_DIM_X to be
    // NUM_BLOCKS
    SplittingConstraint split(LEGION_DIM_X, BLOCK_SIZE);
    LayoutConstraintRegistrar registrar;
    registrar.add_constraint(order).add_constraint(split);
    xcorr_layout = Runtime::preregister_layout(registrar);
  }

  {
    TaskVariantRegistrar registrar(TOP_LEVEL_TASK_ID, "top_level");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    Runtime::preregister_task_variant<top_level_task>(registrar, "top_level");
  }

  {
    TaskVariantRegistrar registrar(WRITE_SAMPLES_TASK_ID, "write_samples");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.add_layout_constraint_set(0, initial_layout);
    registrar.set_leaf();
    Runtime::preregister_task_variant<write_samples_task>(registrar, "write_samples");
  }

  {
    TaskVariantRegistrar registrar(REFORMAT_SAMPLES_TASK_ID, "reformat_samples");
    registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
    registrar.add_layout_constraint_set(0, initial_layout);
    registrar.add_layout_constraint_set(1, xcorr_layout);
    registrar.set_leaf();
    Runtime::preregister_task_variant<reformat_samples_task>(registrar, "reformat_samples");
  }
  return Runtime::start(argc, argv);
}
